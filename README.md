# Amazon Lambda Connector
The Lambda Developer Guide provides additional information. For the service overview, see "https://docs.aws.amazon.com/lambda/latest/dg/welcome.html" What is Lambda, and for information about how the service works, see "https://docs.aws.amazon.com/lambda/latest/dg/lambda-introduction.html" Lambda: How it Works in the Lambda Developer Guide.

Documentation: https://docs.aws.amazon.com/lambda

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/lambda/2015-03-31/openapi.yaml
## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

