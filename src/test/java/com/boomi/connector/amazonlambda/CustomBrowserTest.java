// Copyright (c) 2024 Boomi, LP

package com.boomi.connector.amazonlambda;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.openapi.OpenAPIConnection;
import com.boomi.connector.testutil.SimpleAtomConfig;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.util.LogUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.StringUtil;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

public class CustomBrowserTest {

    private static final Logger LOG = LogUtil.getLogger(CustomBrowserTest.class);
    private static final String SPEC = "openapi-spec-amazonlambda.yaml";
    private final Map<String, Object> connProps;

    int endPointCount = 0;
    int errorCount = 0;
    int stackoverflowCount = 0;
    int otherErrorCount = 0;
    ArrayList<String> erroringOperationIds = new ArrayList<>();

    final String[] httpMethods = {
            "GET", "POST", "PUT", "DELETE", "PATCH", "HEAD", "OPTIONS", "TRACE" };

    public CustomBrowserTest() {
        connProps = new HashMap<>();
        connProps.put("spec", SPEC);
    }

    @Test
    public void testConnectionSuccess() throws IOException {
        try (MockedStatic<HttpClientBuilder> clientBuilder = mockStatic(HttpClientBuilder.class)) {
            CustomBrowser browser = getBrowser();
            setupHttpClientMock(clientBuilder, 200, "OK Success", StreamUtil.EMPTY_STREAM);
            browser.testConnection();
        }
    }

    @Test
    public void testConnectionErrorWithoutErrorPayload() throws IOException {
        try (MockedStatic<HttpClientBuilder> clientBuilder = mockStatic(HttpClientBuilder.class)) {
            CustomBrowser browser = getBrowser();
            setupHttpClientMock(clientBuilder, 403, "Something happened", null);
            Assertions.assertThrows(ConnectorException.class, browser::testConnection);
        }
    }

    @Test
    public void testConnectionErrorWithErrorPayload() throws IOException {
        try (MockedStatic<HttpClientBuilder> clientBuilder = mockStatic(HttpClientBuilder.class)) {
            CustomBrowser browser = getBrowser();
            setupHttpClientMock(clientBuilder, 500, "Something happened",
                    new ByteArrayInputStream("something happened".getBytes(StringUtil.UTF8_CHARSET)));
            Assertions.assertThrows(ConnectorException.class, browser::testConnection);
        }
    }


    @Test
    void testConnectionWithHeaders() throws IOException {
        ArgumentCaptor<HttpGet> requestCaptor = ArgumentCaptor.forClass(HttpGet.class);
        CustomBrowser browser = getBrowser();
        OpenAPIConnection<? extends BrowseContext> connection = browser.getConnection();

        Map<String, String> customHeaders = new HashMap<>();
        customHeaders.put("headerKey", "headerValue");
        when(connection.getContext().getConnectionProperties().getCustomProperties("customHeaders")).thenReturn(
                customHeaders);

        try (MockedStatic<HttpClientBuilder> clientBuilder = mockStatic(HttpClientBuilder.class)) {
            setupHttpClientMock(requestCaptor, clientBuilder, 200, "OK Success", StreamUtil.EMPTY_STREAM);
            browser.testConnection();
        }

        HttpGet request = requestCaptor.getValue();
        Header[] headers = request.getHeaders("headerKey");

        Assertions.assertNotNull(headers);
        Assertions.assertEquals(1, headers.length);
        Assertions.assertEquals("headerValue", headers[0].getValue());
    }

    private static void setupHttpClientMock(MockedStatic<HttpClientBuilder> clientBuilder, int statusCode,
            String reasonPhrase, InputStream payload) throws IOException {
        setupHttpClientMock(ArgumentCaptor.forClass(HttpGet.class), clientBuilder, statusCode, reasonPhrase, payload);
    }

    private static void setupHttpClientMock(ArgumentCaptor<HttpGet> requestCaptor,
            MockedStatic<HttpClientBuilder> clientBuilder, int statusCode, String reasonPhrase, InputStream payload)
            throws IOException {

        HttpClientBuilder clientBuilderNotStatic = Mockito.mock(HttpClientBuilder.class);
        CloseableHttpClient clientMock = Mockito.mock(CloseableHttpClient.class);
        when(clientBuilderNotStatic.build()).thenReturn(clientMock);

        clientBuilder.when(HttpClientBuilder::create).thenReturn(clientBuilderNotStatic);

        StatusLine line = mock(StatusLine.class);
        when(line.getReasonPhrase()).thenReturn(reasonPhrase);
        when(line.getStatusCode()).thenReturn(statusCode);

        HttpEntity entity = mock(HttpEntity.class);
        when(entity.getContent()).thenReturn(payload);
        when(entity.getContentLength()).thenReturn(payload == null ? 0L : 1L);

        CloseableHttpResponse response = mock(CloseableHttpResponse.class);
        when(clientMock.execute(requestCaptor.capture(), any(HttpClientContext.class))).thenReturn(response);
        when(response.getEntity()).thenReturn(entity);
        when(response.getStatusLine()).thenReturn(line);
    }

    private static CustomBrowser getBrowser() {
        CustomConnection connection = Mockito.mock(CustomConnection.class, RETURNS_DEEP_STUBS);
        when(connection.getContext().getConnectionProperties().getProperty("awsAccessKey")).thenReturn(
                "myAccessKeyMock");
        when(connection.getContext().getConnectionProperties().getProperty("awsSecretKey")).thenReturn(
                "mySecretKeyMock");
        when(connection.getContext().getConnectionProperties().getProperty("url")).thenReturn(
                "https://lambda.us-east-1.amazonaws.com");
        when(connection.getContext().getConnectionProperties().getProperty("awsRegion")).thenReturn("us-east-1");

        return new CustomBrowser(connection);
    }

    @Test()
    public void testTypes() {
        CustomConnector connector = new CustomConnector();

        for (String httpMethod : httpMethods) {
            BrowseContext browseContext = new SimpleBrowseContext(new SimpleAtomConfig(), connector, null, httpMethod,
                    connProps, null);

            Browser browser = connector.createBrowser(browseContext);
            try {
                browser.getObjectTypes();
            } catch (Exception e) {
                if (e.getMessage() == null || !e.getMessage().contains("no types available")) {
                    throw e;
                }
            }
        }
    }

    @Test()
    public void testProfiles() {
        for (String httpMethod : httpMethods) {
            browseHttpMethod(httpMethod);
        }

        String message = String.format("%d out of %d endpoints failed.", errorCount, endPointCount);
        LOG.log(Level.INFO, message);
        message = String.format("%d of those are stackoverflow errors.", stackoverflowCount);
        LOG.log(Level.INFO, message);
        message = String.format("%d of those are other errors.", otherErrorCount);
        LOG.log(Level.INFO, message);

        LOG.log(Level.INFO, "List of erroring operation ids:");
        for (String erroringOperationId : erroringOperationIds) {
            LOG.log(Level.INFO, erroringOperationId);
        }
    }

    private void browseHttpMethod(String httpMethod) {
        CustomConnector connector = new CustomConnector();
        BrowseContext browseContext = new SimpleBrowseContext(new SimpleAtomConfig(), connector, null, httpMethod,
                connProps, null);

        Browser browser = connector.createBrowser(browseContext);
        ObjectTypes objectTypes = new ObjectTypes();
        try {
            objectTypes = browser.getObjectTypes();
        } catch (Exception e) {
            if (e.getMessage() == null || !e.getMessage().contains("no types available")) {
                throw e;
            }
        }
        for (ObjectType objectType : objectTypes.getTypes()) {
            endPointCount++;
            String path = objectType.getId();
            String operationId = objectType.getLabel();
            Collection<ObjectDefinitionRole> roles = new HashSet<>();
            roles.add(ObjectDefinitionRole.INPUT);
            roles.add(ObjectDefinitionRole.OUTPUT);

            try {
                browser.getObjectDefinitions(path, roles);
            } catch (StackOverflowError e) {
                String message = String.format("Stackoverflow error for operationId %s", operationId);
                LOG.log(Level.WARNING, message);
                erroringOperationIds.add(operationId);
                errorCount++;
                stackoverflowCount++;
            } catch (Exception e) {
                String message = String.format(
                        "Browser failing for path: %s, http method: %s, operation id: %s %n error: %s", path,
                        httpMethod, operationId, e.getMessage());
                LOG.log(Level.WARNING, message);
                erroringOperationIds.add(operationId);
                errorCount++;
                otherErrorCount++;
            }
        }
    }
}
