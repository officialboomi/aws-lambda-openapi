// Copyright (c) 2024 Boomi, LP

package com.boomi.connector.amazonlambda;

import com.boomi.common.rest.authentication.AuthenticationType;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.openapi.OpenAPIOperationConnection;

public class CustomOperationConnection extends OpenAPIOperationConnection {

    private static final String AUTHENTICATION_TYPE = "AWS";
    public static final String AWS_SERVICE = "lambda";

    public CustomOperationConnection(OperationContext context) {
        super(context);
    }

    @Override
    public boolean getPreemptive() {
        return true;
    }

    @Override
    public String getAWSService() {
        return AWS_SERVICE;
    }

    @Override
    public AuthenticationType getAuthenticationType() {
        return AuthenticationType.fromValue(AUTHENTICATION_TYPE);
    }
}
