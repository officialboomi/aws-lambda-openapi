// Copyright (c) 2024 Boomi, LP
package com.boomi.connector.amazonlambda.util;

import com.boomi.common.apache.http.impl.CustomProxyRoutePlanner;
import com.boomi.connector.api.AtomProxyConfig;
import com.boomi.connector.api.ConnectorContext;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Utility class to build {@link HttpClient}s configured with proxy settings, provided those are available.
 */

public class HttpClientFactory {

    private HttpClientFactory() {
    }

    /**
     * Builds a {@link CloseableHttpClient} configured with atom proxy settings.
     *
     * @param builder the http client builder
     * @param context the context to get the atom proxy configuration
     * @return a new {@link CloseableHttpClient} instance
     */
    public static CloseableHttpClient create(HttpClientBuilder builder, ConnectorContext context) {
        AtomProxyConfig proxyConfig = context.getConfig().getProxyConfig();
        if (proxyConfig.isProxyEnabled()) {
            builder.setRoutePlanner(new CustomProxyRoutePlanner(proxyConfig));

            if (proxyConfig.isAuthenticationEnabled()) {
                builder.setDefaultCredentialsProvider(getProxyCredentialsProvider(proxyConfig));
            }
        }

        return builder.build();
    }

    private static CredentialsProvider getProxyCredentialsProvider(AtomProxyConfig proxyConfig) {
        AuthScope proxyAuthScope = new AuthScope(proxyConfig.getProxyHost(),
                Integer.parseInt(proxyConfig.getProxyPort()));

        Credentials proxyCredentials = new UsernamePasswordCredentials(proxyConfig.getProxyUser(),
                proxyConfig.getProxyPassword());

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(proxyAuthScope, proxyCredentials);

        return credentialsProvider;
    }
}
