// Copyright (c) 2024 Boomi, LP

package com.boomi.connector.amazonlambda;

import com.boomi.common.apache.http.auth.HttpAWSSignableRequest;
import com.boomi.common.aws.AWSCredentials;
import com.boomi.common.aws.AWSSignatureException;
import com.boomi.common.aws.AWSV4Signer;
import com.boomi.connector.amazonlambda.util.HttpClientFactory;
import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.openapi.OpenAPIBrowser;
import com.boomi.connector.openapi.OpenAPIConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.StringUtil;

import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Map;

public class CustomBrowser extends OpenAPIBrowser implements ConnectionTester {

    private static final String TEST_CONNECTION_PATH = "/2016-08-19/account-settings/";
    private static final String AWS_REGION_CUSTOM_VALUE = "Custom";
    private static final String AWS_REGION = "awsRegion";
    private static final String AWS_CUSTOM_REGION = "awsCustomRegion";
    private static final String AUTHORIZATION = "Authorization";
    private static final String CUSTOM_HEADERS_PROPERTY = "customHeaders";

    private final String _awsAccessKey;
    private final String _awsSecretKey;
    private final String _awsRegion;
    private final String _awsUrl;

    public CustomBrowser(final OpenAPIConnection connection) {
        super(connection);
        PropertyMap connProps = connection.getContext().getConnectionProperties();

        _awsAccessKey = connProps.getProperty("awsAccessKey");
        _awsSecretKey = connProps.getProperty("awsSecretKey");
        _awsUrl = connProps.getProperty("url");
        _awsRegion = getRegion(connProps);
    }

    private static String getRegion(PropertyMap connProps) {
        String awsRegionValue = connProps.getProperty(AWS_REGION);
        if (AWS_REGION_CUSTOM_VALUE.equals(awsRegionValue)) {
            return connProps.getProperty(AWS_CUSTOM_REGION);
        }
        return awsRegionValue;
    }

    /**
     * Test the connection by requesting the AWS Lambda account settings. If it fails, it throws an exception that will
     * be displayed on the test connection wizard.
     */
    @Override
    public void testConnection() {
        String url = _awsUrl + TEST_CONNECTION_PATH;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try {
            HttpUriRequest request = new HttpGet(url);
            addCustomHeaders(request);
            AWSCredentials awsCredentials = new AWSCredentials(CustomOperationConnection.AWS_SERVICE, _awsRegion,
                    _awsAccessKey, _awsSecretKey);
            HttpClientContext httpClientContext = HttpClientContext.create();

            HttpAWSSignableRequest awsRequest = new HttpAWSSignableRequest(request, awsCredentials, httpClientContext);
            AWSV4Signer.sign(awsRequest);
            awsRequest.addHeader(AUTHORIZATION, awsRequest.getSigningKey());

            client = HttpClientFactory.create(HttpClientBuilder.create(), getContext());
            response = client.execute(request, httpClientContext);

            if (HttpURLConnection.HTTP_OK != response.getStatusLine().getStatusCode()) {
                throw new ConnectorException("Test connection failed: " + getErrorMessage(response));
            }
        } catch (IOException | AWSSignatureException e) {
            throw new ConnectorException("Test connection error", e);
        } finally {
            IOUtil.closeQuietly(response, client);
        }
    }

    private void addCustomHeaders(HttpMessage request) {
        Map<String, String> customHeaders = getConnection().getContext().getConnectionProperties().getCustomProperties(
                CUSTOM_HEADERS_PROPERTY);
        for (Map.Entry<String, String> entry : customHeaders.entrySet()) {
            request.addHeader(entry.getKey(), entry.getValue());
        }
    }

    private static String getErrorMessage(HttpResponse response) throws IOException {

        if ((response.getEntity() == null) || (response.getEntity().getContentLength() == 0L)) {
            return response.getStatusLine().getReasonPhrase();
        }

        InputStream inputStream = response.getEntity().getContent();

        try {
            return StreamUtil.toString(inputStream, StringUtil.UTF8_CHARSET);
        } finally {
            IOUtil.closeQuietly(inputStream);
        }
    }
}
