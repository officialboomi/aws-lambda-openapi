<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.boomi.connector</groupId>
    <artifactId>openapiconnector-aws_lambda</artifactId>
    <version>1.0.5-SNAPSHOT</version>

    <!-- Specification of the Boomi SDK repository -->
    <repositories>
        <repository>
            <id>boomisdk</id>
            <name>Connector SDK Repository</name>
            <url>https://boomisdk.s3.amazonaws.com/releases</url>
        </repository>
    </repositories>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <connector.sdk.version>2.19.0</connector.sdk.version>
        <jackson.version>2.12.4</jackson.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.release.plugin.version>3.0.0-M6</maven.release.plugin.version>
        <junit.jupiter.version>5.10.1</junit.jupiter.version>
        <mockito.junit.jupiter.version>4.8.0</mockito.junit.jupiter.version>
        <mockito.inline.version>5.2.0</mockito.inline.version>
    </properties>

    <scm>
        <connection>scm:git:file://${env.PWD}/.git</connection>
        <tag>HEAD</tag>
    </scm>

    <dependencies>
        <!-- SDK Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-api</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.util</groupId>
            <artifactId>boomi-json-util</artifactId>
            <version>1.1.10</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>io.swagger.parser.v3</groupId>
            <artifactId>swagger-parser-v3</artifactId>
            <version>2.1.13</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jsr310</artifactId>
            <version>2.15.1</version>
        </dependency>
        <dependency>
            <groupId>com.boomi.util</groupId>
            <artifactId>boomi-util</artifactId>
            <version>2.3.12</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-util</artifactId>
            <version>${connector.sdk.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.5.13</version>
        </dependency>
        <!-- OpenAPI Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-openapi</artifactId>
            <version>${connector.sdk.version}</version>
            <!-- exclusions that are not needed or included by the atom-->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.httpcomponents</groupId>
                    <artifactId>httpclient</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-databind</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.json</groupId>
                    <artifactId>json</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>commons-codec</groupId>
                    <artifactId>commons-codec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.noelios.restlet</groupId>
                    <artifactId>com.noelios.restlet.ext.net</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.restlet</groupId>
                    <artifactId>org.restlet</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.code.findbugs</groupId>
                    <artifactId>jsr305</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>javax.mail</groupId>
                    <artifactId>mailapi</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- Test Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-test-util</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.json</groupId>
                    <artifactId>json</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>commons-codec</groupId>
                    <artifactId>commons-codec</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>junit</groupId>
                    <artifactId>junit</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20131018</version>
        </dependency>

        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>1.15</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jackson.version}</version>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <version>${mockito.junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-inline</artifactId>
            <version>${mockito.inline.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-model</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources/META_INF/</directory>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <includes>
                        <include>com/boomi/connector/amazonlambda/**/*.java</include>
                    </includes>
                </configuration>
            </plugin>

            <!-- CAR Assembly File Config -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration combine.self="override">
                    <descriptors>
                        <descriptor>src/assembly/assembly.xml</descriptor>
                    </descriptors>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <systemPropertyVariables>
                        <javax.xml.accessExternalSchema>all</javax.xml.accessExternalSchema>
                    </systemPropertyVariables>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven.release.plugin.version}</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <scmReleaseCommitComment>@{prefix} - release tag: '@{releaseLabel}'</scmReleaseCommitComment>
                    <scmDevelopmentCommitComment>[skip ci] @{prefix} - increment version for next development
                        iteration
                    </scmDevelopmentCommitComment>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>